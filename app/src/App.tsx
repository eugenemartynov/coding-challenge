import React from 'react';
import Companies from './screens/companies';

function App() {
  return (
    <div className="bg-gray-100 py-20">
      <div className="max-w-5xl mx-auto sm:px-6 lg:px-8">
        <Companies />
      </div>
    </div>
  );
}

export default App;
