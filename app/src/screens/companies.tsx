import React from 'react';
import SearchToolbar from '../components/search-toolbar';
import CompanyList from '../components/company-list';
import { SearchDto } from '../dtos/search.dto';
import { useCompanies } from '../hooks/use-companies';
import ErrorMessage from '../components/error-message';
import debounceFn from 'debounce-fn';

function Companies() {
  const { companies, setSearch, status } = useCompanies();

  const debouncedSetSearch = React.useMemo(() => debounceFn(setSearch, { wait: 300 }), [setSearch]);

  function onSearch({ searchTerm, filter }: SearchDto) {
    debouncedSetSearch({ searchTerm, filter });
  }

  return (
    <>
      <div>
        <SearchToolbar isLoading={status === 'pending'} onSearch={onSearch} />
      </div>
      <div className="mt-5">{status === 'error' ? <ErrorMessage /> : <CompanyList companies={companies} />}</div>
    </>
  );
}

export default Companies;
