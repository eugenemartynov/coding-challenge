const apiURL = process.env.REACT_APP_API_URL;

interface Config {
  data?: any;
  headers?: HeadersInit;
}

async function client(endpoint: string, { data, headers: customHeaders, ...customConfig }: Config = {}) {
  const config: RequestInit = {
    method: data ? 'POST' : 'GET',
    body: data ? JSON.stringify(data) : undefined,
    headers: {
      ...(data && { 'Content-Type': 'application/json' }),
      ...customHeaders,
    },
    ...customConfig,
  };

  return window.fetch(`${apiURL}${endpoint}`, config).then(async response => {
    const data = await response.json();
    if (response.ok) {
      return data;
    } else {
      return Promise.reject(data);
    }
  });
}

export { client };
