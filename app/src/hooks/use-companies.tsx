import { useEffect, useState } from 'react';
import { CompanyDto } from '../dtos/company.dto';
import { SearchDto } from '../dtos/search.dto';
import { client } from '../utils/api-client';

export type Status = 'success' | 'error' | 'pending';

export function useCompanies() {
  const [companies, setCompanies] = useState<CompanyDto[]>([]);
  const [status, setStatus] = useState<Status>('success');
  const [{ searchTerm, filter }, setSearch] = useState<SearchDto>({});

  useEffect(() => {
    setStatus('pending');

    const searchParams = new URLSearchParams();
    filter?.forEach(f => searchParams.append('filter', f));
    if (searchTerm) {
      searchParams.append('term', searchTerm);
    }

    client(`api/companies/?${searchParams.toString()}`).then(
      data => {
        setStatus('success');
        setCompanies(data);
      },
      () => {
        setStatus('error');
      },
    );
  }, [searchTerm, filter]);

  return { companies, setSearch, status };
}
