export interface SearchDto {
  searchTerm?: string;
  filter?: string[];
}
