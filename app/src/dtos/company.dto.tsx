export interface CompanyDto {
  _id: string;
  name: string;
  logo: string;
  city: string;
  specialities: string[];
}
