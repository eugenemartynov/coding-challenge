import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import { faSearch, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { useForm } from 'react-hook-form';
import { SearchDto } from '../dtos/search.dto';

interface SearchToolbarProps {
  onSearch: (request: SearchDto) => void;
  isLoading: boolean;
}

// Normally should be served from API, for the sake of simplicity let's leave it here
const SPECIALITIES = ['Excavation', 'Plumbing', 'Electrical'];

function SearchToolbar({ onSearch, isLoading }: SearchToolbarProps) {
  const { register, watch } = useForm<SearchDto>({
    defaultValues: {
      searchTerm: '',
      filter: [],
    },
  });

  useEffect(() => {
    const subscription = watch(({ searchTerm, filter }) => {
      onSearch({ searchTerm, filter: filter as string[] });
    });

    return () => subscription.unsubscribe();
  }, [onSearch, watch]);

  return (
    <form className="grid grid-cols-1 sm:grid-cols-2 gap-1 p-3 border border-gray-200 sm:rounded-lg bg-gray-50">
      <div className="mt-1.5">
        {SPECIALITIES.map(s => (
          <label key={s} className="inline-flex items-center mr-2">
            {<input {...register('filter')} value={s.toLocaleLowerCase()} type="checkbox" className="form-checkbox" />}
            <span className="ml-2">{s}</span>
          </label>
        ))}
      </div>
      <div className="sm:justify-self-end">
        <div className="relative rounded-md shadow-sm">
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <span className="text-gray-500 sm:text-sm">
              {isLoading ? <FontAwesomeIcon className="animate-spin" icon={faSpinner} /> : <FontAwesomeIcon icon={faSearch} />}
            </span>
          </div>
          <input
            type="text"
            {...register('searchTerm')}
            className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-8 pr-12 sm:text-sm border-gray-300 rounded-md"
            placeholder="Search"
          />
        </div>
      </div>
    </form>
  );
}

export default SearchToolbar;
