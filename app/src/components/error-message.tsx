import React from 'react';

const ErrorMessage = () => {
  return (
    <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
      <strong className="font-bold mr-1">Uh oh...</strong>
      <span className="block sm:inline">There's a problem. Try refreshing the app.</span>
    </div>
  );
};

export default ErrorMessage;
