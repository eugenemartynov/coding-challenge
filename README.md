# Coding Challenge 

## Quick Start

In the project directory, you can run (Docker is required):

### `docker-compose up --build`
Runs the app in the production mode.
Open http://localhost:2021 to view it in the browser.

### `docker-compose down -v`
Stops and removes containers, networks, images, and volumes.

## Description

![img.png](readme/img2.png)

I tried to develop this challenge as close to the real-life codding as possible, given the time constraints and lack of project infrastructure.

All functional requirements are met, but obviously a lot of work or improvements could have been made. 

#### Current restrictions, possible improvements:
- A filter with checkboxes will not look good with a large amount of options. I would replace them with some multi-select dropdown component.
- There is no pagination, but filtering and searching is done on the backend side in case of a huge amount of data.
- Browser URL should reflect the state of the search (selected filters, current page, etc.).
- No UI frameworks were used, so the design is very basic and has room for improvement.
- List component is a simple HTML table, should be replaced with a mobile friendly solution. 
- Security headers, HTTPS, Proxy configuration and other Production infrastructure are missing or simplified.

This list can go on and on, it all depends on the needs of users, available resources and common sense.

## API

This project was bootstrapped with [TypeScript Express Starter](https://github.com/ljlm0402/typescript-express-starter).

The parts that are not relevant to the challenge have been removed to simplify the demo and the review. Test data will be seeded during the first run.

Express as an API framework, Mongo as the DB. 



#### Main controller:
```typescript
class CompanyController {
  public companyService = new CompanyService();

  public async getCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const searchDto = plainToClass(SearchCompaniesDto, req.query);

      const companies: Company[] = await this.companyService.findAllCompanies(searchDto);

      res.status(200).json(companies);
    } catch (error) {
      next(error);
    }
  }
}
```
#### Search DTO:
```typescript
export class SearchCompaniesDto {
  @IsOptional()
  @MaxLength(50)
  term?: string;

  @IsOptional()
  @MaxLength(50, {
    each: true,
  })
  filter?: FilterType | FilterType[];
}
```
#### Data retrieval:
```typescript
class CompanyService {
  public async findAllCompanies({ term = '', filter = [] }: SearchCompaniesDto): Promise<Company[]> {
    if (!Array.isArray(filter)) {
      filter = [filter];
    }

    const companies: Company[] = await CompanyModel.find({
      name: { $regex: term, $options: 'i' },
      ...(filter.length > 0 && { specialities: { $all: filter } }),
    });

    return companies;
  }
}
```

#### Some Tests:
```typescript
describe('Testing Companies', () => {
  const companies = [...];

  describe('[GET] /companies', () => {
    it('should return companies', async () => {
      const companyRoute = new CompanyRoute();

      CompanyModel.find = jest.fn().mockReturnValue(companies);

      (mongoose as any).connect = jest.fn();
      const app = new App([companyRoute]);

      return request(app.getServer()).get(`${companyRoute.path}`).expect(200, companies);
    });

    it('should filter companies by specialities', async () => {
      ...
    });

    it('should search companies by name', async () => {
      ...
    });
  });
});
```

## Web App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In addition to React, some minimal set of auxiliary libraries has been added:
  
 - react-hook-form
 - tailwindcss
 - debounce-fn
 - fontawesome

This was my first experience with tailwindcss, and I didn't expect such a pollution of DOM with classes. Obviously, UI components/wrappers, pre-built classes could have improved the situation, but were omitted due to time constraints.

Preference is given to named functions for easy debugging and profiling.

![img.png](readme/img.png)

#### Entry Component:
```typescript
function Companies() {
  const { companies, setSearch, status } = useCompanies();

  const debouncedSetSearch = React.useMemo(() => debounceFn(setSearch, { wait: 300 }), [setSearch]);

  function onSearch({ searchTerm, filter }: SearchDto) {
    debouncedSetSearch({ searchTerm, filter });
  }

  return (
    <>
      <div>
        <SearchToolbar isLoading={status === 'pending'} onSearch={onSearch}/>
      </div>
      <div className="mt-5">{status === 'error' ? <ErrorMessage/> : <CompanyList companies={companies}/>}</div>
    </>
  );
}
```
#### Data Retrieval Hook:
```typescript
export type Status = 'success' | 'error' | 'pending';

export function useCompanies() {
  const [companies, setCompanies] = useState<CompanyDto[]>([]);
  const [status, setStatus] = useState<Status>('success');
  const [{ searchTerm, filter }, setSearch] = useState<SearchDto>({});

  useEffect(() => {
    setStatus('pending');

    const searchParams = new URLSearchParams();
    filter?.forEach(f => searchParams.append('filter', f));
    if (searchTerm) {
      searchParams.append('term', searchTerm);
    }

    client(`api/companies/?${searchParams.toString()}`).then(
      data => {
        setStatus('success');
        setCompanies(data);
      },
      () => {
        setStatus('error');
      },
    );
  }, [searchTerm, filter]);

  return { companies, setSearch, status };
}
```
#### Search Form:
```typescript
function SearchToolbar({ onSearch, isLoading }: SearchToolbarProps) {
  const { register, watch } = useForm<SearchDto>({
    defaultValues: {
      searchTerm: '',
      filter: [],
    },
  });

  useEffect(() => {
    const subscription = watch(({ searchTerm, filter }) => {
      onSearch({ searchTerm, filter: filter as string[] });
    });

    return () => subscription.unsubscribe();
  }, [onSearch, watch]);

  return (
    <form className="...">
      <div className="mt-1.5">
        {SPECIALITIES.map(s => (
          ...
            {<input {...register('filter')} value={s.toLocaleLowerCase()} type="checkbox" className="form-checkbox" />}
            ...
        ))}
      </div>
      <div className="sm:justify-self-end">
        ...
              {isLoading ? <FontAwesomeIcon className="animate-spin" icon={faSpinner} /> : <FontAwesomeIcon icon={faSearch} />}
            ...
          <input
            type="text"
            {...register('searchTerm')}
            ...
          />
        ...
      </div>
    </form>
  );
};
```

The CompanyList component is mostly an HTML table and omitted from the README. 
