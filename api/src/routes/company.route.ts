import { Router } from 'express';
import CompanyController from '@controllers/company.controller';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import { SearchCompaniesDto } from '@dtos/search-companies-dto';

class CompanyRoute implements Routes {
  public path = '/companies';
  public router = Router();
  public companyController = new CompanyController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}`,
      validationMiddleware(SearchCompaniesDto, 'query'),
      this.companyController.getCompanies.bind(this.companyController),
    );
  }
}

export default CompanyRoute;
