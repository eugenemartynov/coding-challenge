import { Company } from '@interfaces/company.interface';
import CompanyModel from '@models/company.model';
import { SearchCompaniesDto } from '@dtos/search-companies-dto';

class CompanyService {
  public async findAllCompanies({ term = '', filter = [] }: SearchCompaniesDto): Promise<Company[]> {
    if (!Array.isArray(filter)) {
      filter = [filter];
    }

    const companies: Company[] = await CompanyModel.find({
      name: { $regex: term, $options: 'i' },
      ...(filter.length > 0 && { specialities: { $all: filter } }),
    });

    return companies;
  }
}

export default CompanyService;
