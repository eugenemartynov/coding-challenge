export interface Company {
  _id: string;
  name: string;
  logo: string;
  city: string;
  specialities: FilterType[];
}

export type FilterType = 'excavation' | 'plumbing' | 'electrical';
