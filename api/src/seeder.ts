import * as fs from 'fs';
import CompanyModel from '@models/company.model';
import { logger } from '@utils/logger';

const companies = JSON.parse(fs.readFileSync(`${__dirname}/_data/companies.json`, 'utf-8'));

export const importData = async () => {
  try {
    const existent = await CompanyModel.find();
    if (existent.length > 0) {
      return;
    }
    await CompanyModel.create(companies);
    logger.info('Data Imported...');
  } catch (err) {
    logger.error(err);
  }
};
