import mongoose from 'mongoose';
import request from 'supertest';
import App from '@/app';
import CompanyRoute from '@routes/company.route';
import CompanyModel from '@models/company.model';

describe('Testing Companies', () => {
  const companies = [
    {
      _id: 'qpwoeiruty',
      name: 'Burns Industries',
      logo: 'https://image.freepik.com/free-psd/logo-mockup-grey-wall_35913-2122.jpg',
      specialities: ['excavation', 'plumbing', 'electrical'],
      city: 'Berlin',
    },
    {
      _id: 'alskdjfhg',
      name: 'Dunder Mifflin, Inc.',
      logo: 'https://image.freepik.com/free-psd/black-company-logo-wall_145275-163.jpg',
      specialities: ['excavation'],
      city: 'Berlin',
    },
    {
      _id: 'zmxncbv',
      name: 'E Corp',
      logo: 'https://image.freepik.com/free-psd/logo-mockup-glass-door_35913-2083.jpg',
      specialities: ['plumbing'],
      city: 'Munich',
    },
  ];

  describe('[GET] /companies', () => {
    it('should return companies', async () => {
      const companyRoute = new CompanyRoute();

      CompanyModel.find = jest.fn().mockReturnValue(companies);

      (mongoose as any).connect = jest.fn();
      const app = new App([companyRoute]);

      return request(app.getServer()).get(`${companyRoute.path}`).expect(200, companies);
    });

    it('should filter companies by specialities', async () => {
      const companyRoute = new CompanyRoute();

      CompanyModel.find = <any>(
        jest.fn(({ specialities: { $all: filter } }) => companies.filter(c => c.specialities.some(s => filter.some(f => s.includes(f)))))
      );

      (mongoose as any).connect = jest.fn();
      const app = new App([companyRoute]);

      return request(app.getServer()).get(`${companyRoute.path}?filter=plumbing&filter=electrical`).expect(200, [companies[0], companies[2]]);
    });

    it('should search companies by name', async () => {
      const companyRoute = new CompanyRoute();

      CompanyModel.find = <any>jest.fn(({ name: { $regex: term } }) => companies.filter(c => c.name.toLocaleLowerCase().includes(term)));

      (mongoose as any).connect = jest.fn();
      const app = new App([companyRoute]);

      return request(app.getServer()).get(`${companyRoute.path}?term=miff`).expect(200, [companies[1]]);
    });
  });
});
