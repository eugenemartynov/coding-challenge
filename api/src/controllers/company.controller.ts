import { NextFunction, Request, Response } from 'express';
import { Company } from '@interfaces/company.interface';
import CompanyService from '@services/company.service';
import { SearchCompaniesDto } from '@dtos/search-companies-dto';
import { plainToClass } from 'class-transformer';

class CompanyController {
  public companyService = new CompanyService();

  public async getCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const searchDto = plainToClass(SearchCompaniesDto, req.query);

      const companies: Company[] = await this.companyService.findAllCompanies(searchDto);

      res.status(200).json(companies);
    } catch (error) {
      next(error);
    }
  }
}

export default CompanyController;
