import { IsOptional, MaxLength } from 'class-validator';
import { FilterType } from '@interfaces/company.interface';

export class SearchCompaniesDto {
  @IsOptional()
  @MaxLength(50)
  term?: string;

  @IsOptional()
  @MaxLength(50, {
    each: true,
  })
  filter?: FilterType | FilterType[];
}
