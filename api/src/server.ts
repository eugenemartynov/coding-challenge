import 'dotenv/config';
import App from '@/app';
import CompanyRoute from '@routes/company.route';
import validateEnv from '@utils/validateEnv';

validateEnv();

const app = new App([new CompanyRoute()]);

app.listen();
