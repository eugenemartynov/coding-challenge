import { model, Schema, Document } from 'mongoose';
import { Company } from '@interfaces/company.interface';

const CompanySchema: Schema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  logo: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  specialities: {
    type: [String],
    required: true,
    enum: ['excavation', 'plumbing', 'electrical'],
    index: true,
  },
});

const CompanyModel = model<Company & Document>('Company', CompanySchema);

export default CompanyModel;
